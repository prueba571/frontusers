import clienteAxios from "../config/axios";

export const getCountries = async() => {
    const response = await clienteAxios.get(`countries`);
    return response.data.data;
};

export const getAreas = async() => {
    const response = await clienteAxios.get(`areas`);
    return response.data.data;
};

export const getDocuments = async() => {
    const response = await clienteAxios.get('documents');
    return response.data.data;
};


