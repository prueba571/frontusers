import clienteAxios from "../config/axios";

export const getEmployees = async() => {
    const response = await clienteAxios.get('users');
    return response.data.data;
};

export const getEmployeeById = async(id) => {
    const response = await clienteAxios.get(`users/${id}`);
    return response.data.data;
};

export const store = async(form) => {
    const response = await clienteAxios.post(`users`,form);
    return response.data.data;
};

export const update = async(id,form) => {
    const response = await clienteAxios.put(`users/${id}`,form);
    return response.data;
};


export const deleteEmployee = async(id) => {
    const response = await clienteAxios.delete(`users/${id}`);
    return response.data;
};
