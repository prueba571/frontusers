import axios from 'axios';

const clienteAxios = axios.create({
    baseURL : 'http://localhost:8000/api/'//process.env.REACT_APP_BACKEND_URL
});

export default clienteAxios;