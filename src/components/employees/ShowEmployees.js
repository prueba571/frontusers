import React, {useEffect,useState} from 'react';
import { getEmployees,deleteEmployee } from "../../helpers/requestEmployees";
import {Link} from 'react-router-dom';
import swal from 'sweetalert';


const ShowEmployees = () => {
  
  const [employees, setEmployees] = useState([]);
  const [currentPage, setCurrentPage] = useState(0);
  const [search, setSearch] = useState("");
  const [filterData, setFilterData] = useState([]);
  const [infoResult, setInfoResult] = useState('')

  useEffect( () => {
    getAllEmployees();
  },[currentPage] );
  
  //consulta api get 
  const getAllEmployees =  async()=>{
    const response = await getEmployees();
    setEmployees(response);
    setFilterData(response.slice(currentPage,currentPage+5));
  }
 //consulta api delete
  const deleteOneEmployee = async (id)=>{

    return swal({
      title:'Eliminar',
      text:'Esta seguro de eliminar este registro?',
      icon:'warning',
      buttons:['no','si']
    })
    .then(res=>{
     
      if(res){
        const response = deleteEmployee(id).
        then(response=>{
          if(response.status=="success")
          swal({text:'Registro eliminado con éxito', icon:'success',timer:'2000'})  
          getAllEmployees();
        })
        console.log(response)
       
          
      }
    })


   
  }
  //mensaje
  const msj= ()=>{
  
  }


 //botones paginacion
  const nextPage = ()=>{
    if(filterData.length+5 >= currentPage)
      setCurrentPage(currentPage+5);
  }

  const prevPage = ()=>{
    if(currentPage>0)
      setCurrentPage(currentPage-5);
  }

  //filtrar datos segun busqueda realizada
  const onSearchChange = (e)=>{ 
    setCurrentPage(0);
    if (e.target.value) {
      const textData = e.target.value.toUpperCase();
      const newData = employees.filter((item) => {
          return (
              (item.primer_nombre ? item.primer_nombre.toUpperCase().includes(textData) : '') ||
              (item.primer_apellido ? item.primer_apellido.toUpperCase().includes(textData) : '') ||
              (item.segundo_nombre ? item.segundo_nombre.toUpperCase().includes(textData) : '') ||
              (item.segundo_apellido ? item.segundo_apellido.toUpperCase().includes(textData) : '') ||
              (item.document.nombre ? item.document.nombre.toUpperCase().includes(textData) : '') ||
              (item.country.nombre  ? item.country.nombre  .toUpperCase().includes(textData) : '') ||
              (item.numero_identificacion  ? item.numero_identificacion .toUpperCase().includes(textData) : '') ||
              (item.estado  ? item.estado.toUpperCase().includes(textData) : '')
          )
      });
      setFilterData(newData.slice(currentPage,currentPage+5));
      setSearch(e.target.value);
      setInfoResult('No se encontraron coincidencias');
    } else {
      setFilterData(null);
      setSearch(e.target.value);
      setInfoResult(null);
      setFilterData(employees.slice(currentPage,currentPage+5));
    }
  }

  return (
    <div className='container'>
      <div className='mt-5'>
        <h3>Lista de empleados</h3>
      </div>
      <div className='d-grid gap-2 col-3'>
        <Link to="/create" className='btn btn-success btn-lg mt-2 mb-2 text-white'>Crear nuevo empleado</Link>
      </div>
      <div className="mb-3 col-3" >
        <label htmlFor="" className="form-label"></label>
        <input type="text" className="form-control" value={search || '' } onChange={onSearchChange} placeholder="Buscar..."/>
      </div>
      <table className='table table-dark table-striped'>
        <thead className='bg-primary text-white'>
          <tr>
            <th>Nombre</th>
            <th>Tipo documento</th>
            <th>N° Documento</th>
            <th>Email</th>
            <th>Estado</th>
            <th>País</th>
            <th>Área</th>
            <th>Fecha de ingreso</th>
            <th>Fecha y hora de registro</th>
            <th>Fecha y hora de actualización</th>
          </tr>
        </thead>
        <tbody>
            
          {filterData && filterData.length>0? filterData.map((e) => (
            <tr key={e.id}>
              <td>{e.primer_nombre} {e.segundo_nombre} {e.primer_apellido} {e.segundo_apellido}</td>
              <td>{e.document.nombre}</td>
              <td>{e.numero_identificacion}</td>
              <td>{e.email}</td>
              <td>{e.estado}</td>
              <td>{e.country.nombre}</td>
              <td>{e.area.nombre}</td>
              <td>{e.created_at}</td>
              <td>{e.updated_at}</td>
              <td>
                <Link to={`/edit/${e.id}`} className='btn btn-warning'>Edit</Link>
                <button onClick={()=>deleteOneEmployee(e.id)} className='btn btn-danger'>Delete</button>
              </td>
            </tr>
            
            )):<tr><td>{infoResult}</td></tr>
          }
        </tbody>
      </table>
      <button className='btn btn-primary' onClick={prevPage}>Prev</button>&nbsp;
      <button className='btn btn-primary' onClick={nextPage}>Sig</button>
    </div>
  )
}

export default ShowEmployees