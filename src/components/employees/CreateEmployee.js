import React from 'react'
import FormEmployee from '../form/formEmployee';

function CreateEmployee() {

    return (
        <FormEmployee edit={false}/>
    )
}

export default CreateEmployee