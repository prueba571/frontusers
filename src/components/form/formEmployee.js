import React ,{useState,useEffect}from 'react';
import { useForm } from "../../hook/useForm";
import { getEmployeeById } from "../../helpers/requestEmployees";
import { getDocuments,getAreas,getCountries } from "../../helpers/requestDataSelects";
import { useParams } from "react-router-dom";

const data = {
    primer_nombre:"",
    primer_apellido:"",
    segundo_nombre:"",
    segundo_apellido:"",
    document_id:"",
    numero_identificacion:"",
    area_id:"",
    fecha_ingreso:"",
    estado:"activo",
    country_id:"",
};

const validationsForm = (form) => { 
    let errors = {};
    const regex1 = /^[a-zA-Z]+$/;//permite solo letras de A a la Z sin caracteres especiales ni espacios ni la ñ
    const regex2 = /^[a-zA-Z\s]+$/;//permite solo letras de A a la Z con espacios sin caracteres especiales ni la ñ
    const regex3 = /^.{1,20}$/;//permite maximo 20 caracteres
    const regex4 = /^.{1,50}$/;//permite maximo 50 caracteres
    const regex5 = /^[a-zA-Z0-9]+$/;//permite solo caracteres alfanumericos 
   

    if(!form.primer_nombre.trim()){
        errors.primer_nombre = "El campo 'Primer nombre' es requerido";
    }else if( !regex1.test(form.primer_nombre.trim())){
        errors.primer_nombre = "No se permiten espacios, caracteres especiales ni números";
    }else if(!regex3.test(form.primer_nombre.trim())){
        errors.primer_nombre = "Ingrese máximo 20 caracteres";
    }

    if( !form.primer_apellido.trim()){
        errors.primer_apellido = "El campo 'Primer apellido' es requerido"; 
    }else if( !regex1.test(form.primer_apellido.trim())){
        errors.primer_apellido = "No se permiten espacios, caracteres especiales ni números";
    }else if( !regex3.test(form.primer_apellido.trim())){
        errors.primer_apellido = "Ingrese máximo 20 caracteres";
    }

    if(!form?.segundo_nombre?.trim()){
    }else if( !regex2.test(form.segundo_nombre.trim())){
        errors.segundo_nombre = "No se permiten caracteres especiales ni números";
    }else if( !regex4.test(form.segundo_nombre.trim())){
        errors.segundo_nombre = "Ingrese máximo 50 caracteres";
    }

    if( !form.segundo_apellido.trim()){
        errors.segundo_apellido = "El campo 'Segundo apellido' es requerido";
    }else if( !regex1.test(form.segundo_apellido.trim())){
        errors.segundo_apellido = "No se permiten espacios, caracteres especiales ni números";
    }else if(!regex3.test(form.segundo_apellido.trim())){
        errors.segundo_apellido = "Ingrese máximo 20 caracteres";
    }

    if( !form.document_id){
        errors.document_id = "El campo 'Tipo de documento' es requerido";
    }

    if( !form.numero_identificacion.trim()){
        errors.numero_identificacion = "El campo 'Número de documento' es requerido";
    }else if( !regex3.test(form.numero_identificacion.trim())){
        errors.numero_identificacion = "Ingrese máximo 20 caracteres";
    }else if( !regex5.test(form.numero_identificacion.trim())){
        errors.numero_identificacion = "Ingrese solo números y letras sin espacios";
    }

    if( !form.area_id){
        errors.area_id = "El campo 'Área' es requerido";
    }

    if( !form.fecha_ingreso.trim()){
        errors.fecha_ingreso = "El campo 'Fecha de ingreso' es requerido";
    }

    if( !form.country_id){
        errors.country_id = "El campo 'País' es requerido";
    }

    return errors;
};

const FormEmployee = (edit) => {
    const [initialForm, setInitialForm] = useState(data);
    const [loadingApi, setLoadingApi] = useState(true)
    const [countries, setCountries] = useState([])
    const [areas, setAreas] = useState([])
    const [documents, setDocuments] = useState([])

    const {id} = useParams();
   
    useEffect(() => {
        if(edit.edit){
            getEmployee(id);
            
        }else{
            setInitialForm(data)
        }
    }, [loadingApi])
    
    useEffect(() => {
        getAllCountries()
       
        getAllDocuments()

       
       getAllAreas()
    }, [])
    
    const getEmployee=async(id)=>{
        const response = await getEmployeeById(id)
        setLoadingApi(false)
        setInitialForm(response)
    }
    
    const getAllCountries = async()=>{
        const response = await getCountries();
        setCountries(response);
    }

    const getAllDocuments = async()=>{
        const response = await getDocuments();
        setDocuments(response);
    }

    const getAllAreas = async()=>{
        const response = await getAreas();
        setAreas(response);
    }
    

  const {
    form,
    errors,
    loading,
    response,
    handleChange,
    handleBlur,
    handleSubmit,
  } = useForm(initialForm,validationsForm,edit.edit);
  console.log()
  return (
    
        
    <div className="container">
      <div className="mt-5">
        <h3>{edit.edit ? 'Editar' :'Crear'} empleado</h3>
        <div className="col-auto text-center w-50 p-4">
          <form onSubmit={handleSubmit}>
            <div className="col-auto text-center">
              <div className="mb-3">
                <label htmlFor="" className="form-label">
                  Primer nombre
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="primer_nombre"
                  value={form.primer_nombre}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  aria-describedby="spanId"
                  placeholder="Escriba el primer nombre"
                  required
                />
                {errors.primer_nombre && <span  style={{color:'red'}}>{errors.primer_nombre}</span>}
              </div>
           
              <div className="mb-3">
                <label htmlFor="" className="form-label">
                  Segundo nombre
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="segundo_nombre"
                  value={form.segundo_nombre}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  aria-describedby="spanId"
                  placeholder="Escriba el segundo nombre"
                />
                {errors.segundo_nombre && <span style={{color:'red'}}>{errors.segundo_nombre}</span>}
              </div>
              <div className="mb-3">
                <label htmlFor="" className="form-label">
                  Primer apellido
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="primer_apellido"
                  value={form.primer_apellido}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  aria-describedby="spanId"
                  placeholder="Escriba el primer apellido"
                />
                 {errors.primer_apellido && <span style={{color:'red'}}>{errors.primer_apellido}</span>}
              </div>
              <div className="mb-3">
                <label htmlFor="" className="form-label">
                  Segundo apellido
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="segundo_apellido"
                  value={form.segundo_apellido}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  aria-describedby="spanId"
                  placeholder="Escriba el segundo apellido"
                />
                {errors.segundo_apellido && <span style={{color:'red'}}>{errors.segundo_apellido}</span>}
              </div>
              {documents.length>0 ?
              <div className="mb-3">
                <label htmlFor="" className="form-label">
                  Tipo de documento
                </label>
                <select
                  className="form-control"
                  name="document_id"
                  id=""
                  onChange={handleChange}
                  onBlur={handleBlur}
                  defaultValue={form?.document?.id}
                >
                  <option value=''>Seleccione una opción</option>
                  {documents.map(e=>(
                    <option key={e.id ? e.id : ''} value={e.id ? e.id : ''}>{e.nombre ? e.nombre : ''}</option>
                   ))}
                </select>
                {errors.document_id && <span style={{color:'red'}}>{errors.document_id}</span>}
              </div>
              :''}
              <div className="mb-3">
                <label htmlFor="" className="form-label">
                  Número de documento
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="numero_identificacion"
                  value={form.numero_identificacion}
                  onChange={handleChange}
                  aria-describedby="spanId"
                  placeholder="Escriba el número de documento"
                  onBlur={handleBlur}
                />
                {errors.numero_identificacion && <span style={{color:'red'}}>{errors.numero_identificacion}</span>}
              </div>
              {areas.length>0 ?
              <div className="mb-3">
                <label htmlFor="" className="form-label">
                  Área
                </label>
                <select
                  className="form-control"
                  name="area_id"
                  id=""
                  onChange={handleChange}
                  onBlur={handleBlur}
                  defaultValue={form?.area?.id}
                >
                  <option value="">Seleccione una opción</option>
                  {areas.map(e=>(
                    <option key={e.id ? e.id : ''} value={e.id ? e.id : ''}>{e.nombre ? e.nombre : ''}</option>
                   ))}
                </select>
                {errors.area_id && <span style={{color:'red'}}>{errors.area_id}</span>}
              </div>
              :<></>}
              <div className="mb-3">
                <label htmlFor="" className="form-label">
                  Fecha de ingreso
                </label>
                <input
                  type="date"
                  className="form-control"
                  name="fecha_ingreso"
                  value={form.fecha_ingreso}
                  onChange={handleChange}
                  aria-describedby="spanId"
                  onBlur={handleBlur}
                  disabled={edit.edit}
                />
                {errors.fecha_ingreso && <span style={{color:'red'}}>{errors.fecha_ingreso}</span>}
              </div>
              <div className="mb-3">
                <label htmlFor="" className="form-label">
                  Estado
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="estado"
                  value={form.estado}
                  aria-describedby="spanId"
                  disabled
                />
              </div>
              {countries.length>0 ?
              <div className="mb-3">
                <label htmlFor="" className="form-label">
                  País
                </label>
                <select
                  className="form-control"
                  name="country_id"
                  id=""
                  onChange={handleChange}
                  onBlur={handleBlur}
                  defaultValue={form?.country?.id}
                >
                  <option value="">Seleccione una opción</option>
                  {countries.map(e=>(
                    <option key={e.id ? e.id : ''} value={e.id ? e.id : ''}>{e.nombre ? e.nombre : ''}</option>
                   ))}
                </select>
                {errors.country_id && <span style={{color:'red'}}>{errors.country_id}</span>}
              </div>
                :<></>}
              <button type="submit" className="btn btn-primary">
                Guardar
              </button>
            </div>
          </form>
          {loading && <h5>Procesando...</h5>}
          {response && <h5>Se ha guardado el empleado</h5>}
        </div>
      </div>
    </div>
    
     
  );
};

export default FormEmployee;
