import './App.css';
import { BrowserRouter,Routes,Route } from "react-router-dom";
//components
import ShowEmployees from './components/employees/ShowEmployees';
import CreateEmployee from './components/employees/CreateEmployee';
import EditEmployee from './components/employees/EditEmployee';



function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<ShowEmployees/>}/>
          <Route path='/create' element={<CreateEmployee/>}/>
          <Route path='/edit/:id' element={<EditEmployee/>}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
