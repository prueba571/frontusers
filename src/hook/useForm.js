import {useEffect, useState} from 'react'
import { useNavigate } from "react-router-dom";
import { store,update } from "../helpers/requestEmployees";

export const useForm = (initialForm,validateForm,edit) => {
    const [form, setForm] = useState([]);
    const [errors, setErrors] = useState({});
    const [loading, setLoading] = useState(false);
    const [response, setResponse] = useState(null);
    const navigate =  useNavigate()

    useEffect(() => {
      setForm(initialForm)
    }, [initialForm])
    

    const handleChange = async (e)=>{
        setForm({
            ...form,
            [e.target.name]:e.target.value
        });
        setErrors(validateForm(form));
    }
    const handleBlur = (e)=>{
        handleChange(e);
        setErrors(validateForm(form));
    }
    const handleSubmit = async (e)=>{
        e.preventDefault();
        
        setErrors(validateForm(form));

        if(Object.keys(errors).length === 0){
            setLoading(true);
            const response = edit ?  await update(form.id,form) : await store(form);
            if(response.status == "success"){
                setLoading(false);
                setResponse(true);
                setForm(initialForm);
                setTimeout(() => {
                    setResponse(false)
                    navigate('/');
                }, 2000);
            }
        }else{
            return;
        }
    }
   
    return{
        form,
        errors,
        loading,
        response,
        handleChange,
        handleBlur,
        handleSubmit
    }

}
